# Mini-projet Algo Avancé ISPM 2017 #
**Tic Tac Toe (Morpion) 3x3**, joueur contre ordinateur.

Moteur d'intelligence artificiel : *Minimax*

**Professeur** : Mr RABOANARY Andry Heriniaina

**Classe** : ESIIA 4

## Techonologies
+   Unity
+   C#

## Membres du groupe
+   ANDRIANAVALONA Seheno
+   RAKOTO-PARSON Andrianina Lydiano
+   RAKOTONDRAJOA Safidy
+   RAMAHOLIMIHASO Samuel Johary