﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

    // Masques
    private const int TURN = 0x200;
    private const int PLAYER = 0x400;

    private const int PAWN = 0x1FF;
    private const int LINE1 = 0x007;
    private const int LINE2 = 0x038;
    private const int LINE3 = 0x1C0;
    private const int COL1 = 0x049;
    private const int COL2 = 0x092;
    private const int COL3 = 0x124;
    private const int DIAG1 = 0x111;
    private const int DIAG2 = 0x054;

    private int[] MASKS = new int[]
    {
            LINE1,LINE2,LINE3,
            COL1,COL2,COL3,
            DIAG1,DIAG2
        };

    // Pour rappel, on a decider d'adopter la structure ci-après
    // isPlayer | hasTurn | 2,2 | 2,1 | 2,0 | 1,2 | 1,1| 1,0| 0,2| 0,1 | 0,0 
    private int noughtState;
    private int crossState;

    public int Depth { get; set; }
    public int Player { get; set; }

    /**
    Dans tous les cas 
        X : 0
        O : 1
    **/

    public Node(int turn, int depth = 10, int player = 0)
    {
        if (turn == 0) // X commence
        {
            noughtState = 0;
            crossState = TURN;
        }
        else
        {
            noughtState = TURN;
            crossState = 0;
        }
        Depth = depth;
        Player = player;
    }

    public Node(int noughtState, int crossState, int depth = 10, int player = 0) : this(0, depth, player)
    {
        this.noughtState = noughtState;
        this.crossState = crossState;
    }

    public List<Node> GetSuccessors()
    {
        List<Node> successors = new List<Node>();

        int state = (~(noughtState | crossState)) & PAWN;
        //Debug.Log("State : " + Convert.ToString(state, 2));
        bool isFinished = IsFinished();

        int pos = 0;
        while (state > 0 && !isFinished)
        {
            pos = state & -state;
            if (GetTurn() == 0)
            {
                successors.Add(new Node(noughtState ^ TURN, (crossState | pos) ^ TURN, this.Depth, this.Player));
            }
            else
            {
                successors.Add(new Node((noughtState | pos) ^ TURN, crossState ^ TURN, this.Depth, this.Player));
            }
            state ^= pos;
        }

        return successors;
    }

    public int GetTurn()
    {
        return ((noughtState & TURN) == 0) ? 0 : 1;
    }

    public bool IsFull()
    {
        return ((noughtState | crossState) & PAWN) == PAWN;
    }

    public int Eval(int position)
    {
        int value = 0;
        int mask = 0; 
        for (int i = 0; i < MASKS.Length; i++)
        {
            mask = MASKS[i];
            if ((noughtState & mask) == mask)
            {
                return 1 == position ? 100 : -100;
            }
            else if ((crossState & mask) == mask)
            {
                return 0 == position ? 100 : -100;
            }
        }

        return value;
    }

    public int Minimax(int depth, bool maximize)
    {
        if (IsFinished() || depth == 0)
        {
            return Eval(Player == 0 ? 1 : 0);
        }

        int value = 0;
        if (maximize) // Noeud Max : adversaire qui joue
        {
            value = -200;
            foreach (Node node in this.GetSuccessors())
            {
                value = Math.Max(value, node.Minimax(depth - 1, !maximize));
            }
        }

        else // Noeud Min
        {
            value = 200;
            foreach (var node in this.GetSuccessors())
            {
                value = Math.Min(value, node.Minimax(depth - 1, !maximize));
            }
        }

        return value;
    }

    public Node GetBestSuccessor()
    {
        if (IsFinished()) return null;

        Node best = null;
        List<Node> bests = new List<Node>();

        int minimax = this.Minimax(this.Depth, true);
        Debug.Log("Current minimax: " + minimax);
        foreach (var node in this.GetSuccessors())
        {
            if (node.Minimax(Depth - 1, false) == minimax)
            {
                bests.Add(node);
                Debug.Log(node);
            }
        }
        Debug.Log("Bests items count : " + bests.Count);

        int bestChoice = new System.Random().Next(0, bests.Count - 1);
        Debug.Log("Best choice index : " + bestChoice);
        best = bests[bestChoice];
        Debug.Log("Succ minimax: " + best.Minimax(Depth - 1, false));
        Debug.Log("Depth :" + Depth);

        return best;
    }

    public bool IsFinished()
    {
        return IsFull() || (Eval(0) != 0);
    }

    public string GetPawn(int x)
    {
        string value = "";

        if ((noughtState & (1 << x)) != 0)
        {
            value = "O";
        }
        else if ((crossState & (1 << x)) != 0)
        {
            value = "X";
        }

        return value;
    }

    public void SetPawn(int x, int t)
    {
        if (x > 9) return;
        if (0 == t)
        {
            crossState &= (1 << x);
        }
        else if (1 == t)
        {
            noughtState &= (1 << x);
        }
        FlipTurn();
    }

    public void FlipTurn()
    {
        noughtState ^= TURN;
        crossState ^= TURN;
    }

    public Node DoClick(int x)
    {
        Node node = null;
        if (GetTurn() == 0)
        {
            node = new Node(noughtState ^ TURN, (crossState | (1 << x)) ^ TURN, Depth, Player);
        }
        else
        {
            node = new Node((noughtState | (1 << x)) ^ TURN, crossState ^ TURN, Depth, Player);
        }
        return node;
    }

    public override string ToString()
    {
        string s = "";
        int shift = 0;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                shift = 3 * i + j;
                if ((noughtState & (1 << shift)) != 0) s += "O  ";
                else if ((crossState & (1 << shift)) != 0) s += "X  ";
                else s += "   ";
            }
            s += "\n";
        }
        return s;
    }
}
