﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class Player
{
    public Image panel;
    public Text text;
    public Button button;
}

[System.Serializable]
public class PlayerColor
{
    public Color panelColor;
    public Color textColor;
}

public class GameController : MonoBehaviour {
    public Text[] buttonList;
    private string playerSide;
    public GameObject gameOverPanel;
    public Text gameOverText;
    private int moveCount;
    private bool isFinished;

    public GameObject restartButton;

    public Player playerX;
    public Player playerO;
    public PlayerColor activePlayerColor;
    public PlayerColor inactivePlayerColor;

    public GameObject startInfo;

    public Dropdown difficultyChoice;

    private Node currentNode;

    void SetGameControllerReferenceOnButtons()
    {
        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].GetComponentInParent<GridSpace>().SetGameControllerReference(this);
        }
    }

    void Awake()
    {
        currentNode = new Node(0);
        SetGameControllerReferenceOnButtons();
        gameOverPanel.SetActive(false);
        moveCount = 0;
        restartButton.SetActive(false);
        isFinished = false;
    }

    public string GetPlayerSide()
    {
        return playerSide;
    }

    public void EndTurn(int x)
    {
        moveCount++;
        currentNode = currentNode.DoClick(x);
        Node bestSuccessor = currentNode.GetBestSuccessor();
        if (bestSuccessor != null)
        {
            currentNode = bestSuccessor;
            ChangeSides();
        }
        UpdateUI();

        if (currentNode.Eval(0) != 0)
        {
            GameOver(playerSide);
        }
        
        else if (currentNode.IsFinished() && currentNode.Eval(0) == 0)
        {
            GameOver("draw");
        }
        else
        {
            ChangeSides();
        }        
    }

    void GameOver(string winningPlayer)
    {
        SetBoardInteractable(false);
        if (winningPlayer == "draw")
        {
            SetGameOverText("It's a Draw!");
        }
        else
        {
            SetGameOverText(winningPlayer + " Wins");
        }
        SetPlayerColorsInactive();
 
        isFinished = true;
        restartButton.SetActive(true);
    }

    void ChangeSides()
    {
        playerSide = (playerSide == "X") ? "O" : "X";
        if (playerSide == "X")
        {
            SetPlayerColors(playerX, playerO);
        }
        else
        {
            SetPlayerColors(playerO, playerX);
        }
    }

    void SetGameOverText(string value)
    {
        gameOverPanel.SetActive(true);
        gameOverText.text = value;
    }

    public void RestartGame()
    {
        moveCount = 0;
        gameOverPanel.SetActive(false);
        isFinished = false;
        
        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].text = "";
        }
        restartButton.SetActive(false);
        SetPlayerColorsInactive();
        SetPlayerButtons(true);
        startInfo.SetActive(true);
        difficultyChoice.interactable = true;
    }

    void SetBoardInteractable(bool toggle)
    {
        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].GetComponentInParent<Button>().interactable = toggle;
        }
    }

    void SetPlayerColors(Player newPlayer, Player oldPlayer)
    {
        newPlayer.panel.color = activePlayerColor.panelColor;
        newPlayer.text.color = activePlayerColor.textColor;
        oldPlayer.panel.color = inactivePlayerColor.panelColor;
        oldPlayer.text.color = inactivePlayerColor.textColor;
    }

    public void SetStartingSide(string startingSide)
    {
        playerSide = startingSide;
        if (playerSide == "X")
        {
            SetPlayerColors(playerX, playerO);
        }
        else
        {
            SetPlayerColors(playerO, playerX);
        }
        StartGame();
    }

    void StartGame()
    {
        SetPlayerButtons(false);
        SetBoardInteractable(true);
        startInfo.SetActive(false);
        difficultyChoice.interactable = false;

        if (playerSide == "X")
        {
            currentNode = new Node(0, SetDepthFromDifficulty(), 0);
            Debug.Log("Joueur commence");
        }
        else
        {
            currentNode = new Node(0, SetDepthFromDifficulty(), 1);
            currentNode = currentNode.GetBestSuccessor();
            UpdateUI();
            Debug.Log("IA commence");
        }
    }

    public void UpdateUI()
    {
        for (int i = 0; i < buttonList.Length; i++)
        {
            string pawn = currentNode.GetPawn(i);
            Button button = buttonList[i].GetComponentInParent<Button>();
            if (pawn == "")
            {
                button.interactable = true;
            }
            else
            {
                button.interactable = false;
            }
            buttonList[i].text = pawn;
        }
    }

    int SetDepthFromDifficulty()
    {
        switch(difficultyChoice.value)
        {
            case 0: return 3; //easy
            case 1: return 4; //medium
            case 2: return 7; //hard
            case 3: return 10; //unbeatable
                default: return 10;
        }
    }

    void SetPlayerButtons(bool toggle)
    {
        playerX.button.interactable = toggle;
        playerO.button.interactable = toggle;
    }

    void SetPlayerColorsInactive()
    {
        playerX.panel.color = inactivePlayerColor.panelColor;
        playerX.text.color = inactivePlayerColor.textColor;
        playerO.panel.color = inactivePlayerColor.panelColor;
        playerO.text.color = inactivePlayerColor.textColor;
    }

    public void DifficultyChoiceChanged()
    {
        Debug.Log("" + difficultyChoice.value);
    }
}
